import React, { Component } from 'react';
import { Text,
        View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './containers/Home/HomeScreen';
import Icon from 'react-native-vector-icons/FontAwesome';

class Xinnghi extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator = createStackNavigator({
  Home: HomeScreen
}, {
  navigationOptions: {
    headerTitle: 'Xin nghỉ',
    headerStyle: {
      backgroundColor: 'rgb(255, 215, 0)'
    },
    headerLeft:
      <View style={{paddingLeft: 15}}>
        <Icon name={'angle-left'} size={40} />
      </View>
  }
})

export default Xinnghi;
