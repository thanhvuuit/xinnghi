import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { styles } from './styles';
import Don from '../../components/Don/Don';
import {data} from '../../data';
import AcceptModal from '../../components/Modal/AcceptModal';
import RejectModal from '../../components/Modal/RejectModal';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this._onPressAccept = this._onPressAccept.bind(this);
    this._onPressReject = this._onPressReject.bind(this);
  }

  _onPressAccept = () => {
    this.refs.acceptModal.showAcceptModal();
  }

  _onPressReject = () => {
    this.refs.rejectModal.showRejectModal();
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={data}
          renderItem={({item, index}) => {
            return <Don item={item} index={index} parentFlatList={this} />
          }}
        ></FlatList>
        <AcceptModal ref={'acceptModal'} parentFlatList={this}></AcceptModal>
        <RejectModal ref={'rejectModal'} parentFlatList={this}></RejectModal>
      </View>
    );
  }
}

export default HomeScreen;
