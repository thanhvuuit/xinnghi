export const data = [
  {
    id: 1,
    title: 'Đi trễ',
    employee: 'Nguyễn Ngọc Khánh Vy',
    time: '04/09/2018',
    reason: 'Con bị ốm',
    owner: 'Võ Thiên Thành',
    note: '',
    allow: true
  },
  {
    id: 2,
    title: 'Nghỉ phép',
    employee: 'Lê Thị Thuỳ Trang',
    time: '04/09/2018 - 05/09/2018',
    reason: 'Về quê',
    owner: 'Võ Thiên Thành',
    note: 'Tháng này em nghỉ nhiều rồi.',
    allow: false
  },
  {
    id: 3,
    title: 'Nghỉ bệnh',
    employee: 'Phan Hải Vinh',
    time: '04/09/2018 - 05/09/2018',
    reason: 'bệnh',
    owner: 'Võ Thiên Thành',
    note: 'Em nghỉ ngơi khoẻ rồi vào làm.',
    allow: true
  },
  {
    id: 4,
    title: 'Nghỉ du lịch',
    employee: 'Thái Thanh Vũ',
    time: '04/09/2018 - 05/09/2018',
    reason: 'đi chơi',
    owner: 'Võ Thiên Thành',
    note: 'Lý do không chính đáng.',
    allow: false
  }
];