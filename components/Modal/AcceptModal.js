import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modalbox';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';

class AcceptModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  showAcceptModal = () => {
    this.refs.acceptModal.open();
  }

  closeAcceptModal = () => {
    this.refs.acceptModal.close();
  }

  render() {
    return (
      <Modal
        ref={'acceptModal'}
        style={styles.acceptModal}
        position='center'
        backdrop={true}
        onClosed={() => {
          
        }}
      >  
        <Icon name={'times-circle'} size={25} style={styles.iconClose} onPress={this.closeAcceptModal} />
        <Text style={styles.textContent}>
          Bạn có chấp nhận duyệt đơn xin nghỉ này?
        </Text>
        <View style={styles.hr}>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity style={styles.confirmButton} onPress={this.closeAcceptModal}>
            <Text style={styles.textButton}>XÁC NHẬN</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

export default AcceptModal;
