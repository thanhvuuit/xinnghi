import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modalbox';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';

class RejectModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  showRejectModal = () => {
    this.refs.rejectModal.open();
  }

  closeRejectModal = () => {
    this.refs.rejectModal.close();
  }

  render() {
    return (
      <Modal
        ref={'rejectModal'}
        style={styles.rejectModal}
        position='center'
        backdrop={true}
      >  
        <Icon name={'times-circle'} size={25} style={styles.iconClose} onPress={this.closeRejectModal} />
        <Text style={styles.textContent}>
          Bạn có chấp nhận từ chối đơn xin nghỉ này?
        </Text>
        <TextInput style={styles.reasonInput}
          multiline={true}
          numberOfLines={5}
          autoCorrect={false}
          placeholder={'Lý do'}
          selectionColor={'rgb(206, 205, 130'}
        >
        </TextInput>
        <View style={styles.hr}>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity style={styles.confirmButton} onPress={this.closeRejectModal}>
            <Text style={styles.textButton}>XÁC NHẬN</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

export default RejectModal;
