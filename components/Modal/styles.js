import { StyleSheet, Platform, Dimensions } from 'react-native';

const screen = Dimensions.get('window');
export const styles = StyleSheet.create({
  acceptModal: {
    display: 'flex',
    justifyContent: 'center',
    borderRadius: 6,
    shadowRadius: 10,
    width: screen.width - 80,
    height: 180,
    top: -50,
    backgroundColor: 'rgb(255, 254, 162)'
  },
  rejectModal: {
    display: 'flex',
    justifyContent: 'center',
    borderRadius: 6,
    shadowRadius: 10,
    width: screen.width - 80,
    height: 300,
    top: -50,
    backgroundColor: 'rgb(255, 254, 162)'
  },
  iconClose: {
    position: "absolute",
    top: 15,
    right: 15
  },
  textContent: {
    fontSize: 16,
    marginHorizontal: 50,
    marginTop: 50,
    marginBottom: 18,
    textAlign: 'center'
  },
  reasonInput: {
    margin: 15,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgb(206, 205, 130)',
    height: 100,
    paddingHorizontal: 15,
    // paddingTop: 10,
    fontSize: 14
  },
  hr: {
    width: 100 + '%',
    borderBottomWidth: 3,
    borderBottomColor: 'rgb(206, 205, 130)',
  },
  footer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  confirmButton: {
    backgroundColor: 'rgb(255, 215, 0)',
    borderRadius: 5,
    paddingHorizontal: 25,
    paddingVertical: 10,
    shadowRadius: 3,
    shadowOpacity: 0.3,
    shadowOffset: {width: 0, height: 0},
  },
  textButton: {
    fontSize: 16,
    fontWeight: '600'
  }
});