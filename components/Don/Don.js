import React, {Component} from 'react';
import {View, Text, ListView, TouchableHighlight} from 'react-native';
import {styles} from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import Swipeout from 'react-native-swipeout';

class Don extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    const settings = {
      autoClose: true,
      buttonWidth: 100,
      onClose: (secId, rowId, direction) => {

      },
      onOpen: (secId, rowId, direction) => {

      },
      rowId: this.props.item.id,
      right: [
                {
                  component: (
                    <View
                      style={styles.actionButton}
                    >
                      <Icon name={'check'} size={30} />
                    </View>
                  ),
                  backgroundColor: 'rgb(255, 215, 0)',
                  color: '#000000',
                  onPress: () => {
                    this.props.parentFlatList.refs.acceptModal.showAcceptModal();
                  },
                },
                {
                  component: (
                    <View
                      style={styles.actionButton}
                    >
                      <Icon name={'times'} size={30} />
                    </View>
                  ),
                  backgroundColor: 'red',
                  color: '#000000',
                  onPress: () => {
                    this.props.parentFlatList.refs.rejectModal.showRejectModal();
                  }
                },
              ]};
    return (
      <Swipeout
        {...settings}
        style={styles.swipeoutContainer}
        key={this.props.index}
      >
        <View>
          <View style={styles.container}>
            <View style={styles.header}>
              <Text style={styles.headerText}>{this.props.item.title}</Text>
              <Text style={{color: this.props.item.allow ? '#000000' : 'red'}}>
                {this.props.item.allow ? 'ĐÃ DUYỆT' : 'KHÔNG DUYỆT'}
              </Text>
            </View>
            <View style={styles.hr} />
            <View style={styles.content}>
              <View style={styles.contentItem}>
                <Icon name={'user'} size={20} style={styles.icon} />
                <Text style={[styles.contentTextItem, {fontWeight: '500'}]}>
                  {this.props.item.employee}
                </Text>
              </View>
              <View style={styles.contentItem}>
                <Icon5 name={'clock'} size={20} style={styles.icon} />
                <Text style={styles.contentTextItem}>
                  {this.props.item.time}
                </Text>
              </View>
              <View style={styles.contentItem}>
                <Icon name={'info-circle'} size={20} style={styles.icon} />
                <Text style={styles.contentTextItem}>
                  {this.props.item.reason}
                </Text>
              </View>
              <View style={styles.contentItem}>
                <Icon name={'user-check'} size={20} style={styles.icon} />
                <Text style={styles.contentTextItem}>
                  {this.props.item.owner}
                </Text>
              </View>
              {this.props.item.note !== ''
                ? <View style={styles.contentItem}>
                    <Icon
                      name={'exclamation-circle'}
                      size={20}
                      style={styles.icon}
                    />
                    <Text style={styles.contentTextItem}>
                      {this.props.item.note}
                    </Text>
                  </View>
                : <View />}
            </View>
          </View>
        </View>
      </Swipeout>
    );
  }
}

export default Don;
