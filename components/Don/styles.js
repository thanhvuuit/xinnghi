import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  swipeoutContainer: {marginVertical: 4, backgroundColor: 'rgb(252, 252, 252)'},
  container: {
    width: 100 + '%',
    backgroundColor: 'rgb(255, 255, 255)',
    paddingHorizontal: 16,
    paddingVertical: 5,
    marginVertical: 4,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: {width: 0, height: 0},
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 7,
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  hr: {
    width: 100 + '%',
    borderBottomWidth: 3,
    borderBottomColor: 'rgb(210, 210, 210)',
  },
  content: {
    paddingVertical: 15
  },
  contentItem: {
    paddingVertical: 3,
    display: 'flex',
    flexDirection: 'row',
    // backgroundColor: 'red',
    lineHeight: 20
  },
  icon: {
    color: 'rgb(161, 161, 161)',
    fontWeight: '100',
    width: 20,
    top: -3
  },
  contentTextItem: {
    paddingLeft: 14
  },
  actionButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  }
});